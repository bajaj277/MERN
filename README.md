# MERN




## GREYCAMPUS

### 1. [Game](https://bajaj277.github.io/MERN/GreyCampus%20project/)
<p>This site contains a game named as "match the colors". 
  Its small game.</p>



## CipherSchools

### 1. [assignment-1](https://bajaj277.github.io/MERN/assignment%201/)
<p> This site is starting of my new journey towards full stack development. 
  It have some pics and buttons.</p>


### 2. [assignment-2](https://bajaj277.github.io/MERN/assignment%202/)
<p>This is small site in which there is table containing some pics with their description. 
  Below table there are 4 radio buttons which onClick sets the wallpaper to selected option.</p>


### 3. [assignment-3](https://bajaj277.github.io/MERN/assignment%203/)
<p>This is small website containing LOGIN and SIGN-UP pages with it too. 
  This site is responsive.</p>


### 4. [assignment-4](https://bajaj277.github.io/MERN/assignment%204/)
<p>This website is similar to last one but now it contains navigation bar and bunch of other options with it. 
  This is HOLLYWOOD-MOVIES/BOLLYWOOD-MOVIES/ANIME downloading website. 
  All download links are redirected to MOVIESVERSE website.</p>


### 5. [assignment-5](https://bajaj277.github.io/MERN/assignment%205/)
<p>This webpage is copy/clone of github sign in page.</p>


### 6. [assignment-6](https://bajaj277.github.io/MERN/assignment%206/)
<p>Online Calculator</p>
